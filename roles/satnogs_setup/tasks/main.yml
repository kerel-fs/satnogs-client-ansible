---
- name: Create needrestart directory for Raspbian
  ansible.builtin.file:
    path: '/etc/needrestart/conf.d'
    state: 'directory'
    mode: '0755'
  when: ansible_lsb.id == 'Raspbian'
  become: true
- name: Configure needrestart for Raspbian
  ansible.builtin.copy:
    src: 'etc/needrestart/conf.d/raspbian_kernel_filter.conf'
    dest: '/etc/needrestart/conf.d/raspbian_kernel_filter.conf'
    mode: 0644
  when: ansible_lsb.id == 'Raspbian'
  become: true
- name: Remove deprecated satnogs-setup system dependencies
  ansible.builtin.package:
    name: >-
      {{
      satnogs_setup_packages |
      rejectattr("state", "undefined") |
      selectattr("state", "eq", "absent") |
      map(attribute="name") |
      list
      }}
    state: 'absent'
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Install satnogs-setup system dependencies
  ansible.builtin.package:
    name: >-
      {{
      (
      satnogs_setup_packages |
      selectattr("state", "undefined") |
      map(attribute="name") |
      list
      ) +
      satnogs_setup_packages |
      rejectattr("state", "undefined") |
      selectattr("state", "eq", "present") |
      map(attribute="name") |
      list
      }}
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Install Ansible repository keys
  ansible.builtin.apt_key:
    keyserver: 'keyserver.ubuntu.com'
    id: '93C4A3FD7BB9C367'
    state: 'present'
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Install Ansible repository
  ansible.builtin.apt_repository:
    repo: >-
      deb http://ppa.launchpad.net/ansible/ansible/ubuntu
      {{ "trusty" if ansible_lsb.codename == "buster" else "focal" }}
      main
    state: 'present'
    mode: 0644
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Install Ansible package
  ansible.builtin.package:
    name: 'ansible'
    state: 'present'
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Get system Python version  # noqa 306
  ansible.builtin.shell: '/usr/bin/python3 --version 2>&1 | awk ''/^Python/ { print $2 }'''
  become: true
  register: res_system_python_version
  changed_when: false
- name: Get virtualenv Python version  # noqa 306
  ansible.builtin.shell: '{{ satnogs_setup_satnogs_config_venv }}/bin/python --version 2>&1 | awk ''/^Python/ { print $2 }'''
  become: true
  register: res_virtualenv_python_version
  changed_when: false
- name: Remove virtualenv of old Python version
  ansible.builtin.file:
    path: '{{ satnogs_setup_satnogs_config_venv }}'
    state: 'absent'
  when: res_system_python_version.stdout != res_virtualenv_python_version.stdout
  become: true
- name: Install PyPI dependencies
  ansible.builtin.pip:
    name: '{{ satnogs_setup_satnogs_config_pypi }}'
    virtualenv: '{{ satnogs_setup_satnogs_config_venv }}'
    virtualenv_site_packages: true
    virtualenv_python: 'python3'
    editable: false
    extra_args: '--no-deps'
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
- name: Install satnogs-config
  ansible.builtin.pip:
    name: >-
      {{
      satnogs_setup_satnogs_config_url_unstable
      if experimental and satnogs_setup_satnogs_config_url is undefined
      else satnogs_setup_satnogs_config_url | default(satnogs_setup_satnogs_config_name)
      }}
    version: >-
      {{
      satnogs_setup_satnogs_config_version
      if satnogs_setup_satnogs_config_url is undefined and not experimental
      else omit
      }}
    virtualenv: '{{ satnogs_setup_satnogs_config_venv }}'
    virtualenv_site_packages: true
    virtualenv_python: 'python3'
    editable: false
    extra_args: '--no-deps'
    state: '{{ "present" if satnogs_setup_satnogs_config_url is undefined and not experimental else "forcereinstall" }}'
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  register: res
  until: res is success
  become: true
  tags:
    - satnogs_setup_software
- name: Configure satnogs-setup
  ansible.builtin.template:
    src: 'etc/default/satnogs-setup.j2'
    dest: '/etc/default/satnogs-setup'
    mode: 0644
  become: true
  tags:
    - satnogs_setup_software
- name: Create Ansible configuration directory
  ansible.builtin.file:
    path: '/etc/ansible'
    state: 'directory'
    mode: 0755
  become: true
- name: Set up inventory
  ansible.builtin.copy:
    src: 'etc/ansible/hosts'
    dest: '/etc/ansible/hosts'
    mode: 0644
  become: true
- name: Create host variables directory
  ansible.builtin.file:
    path: '/etc/ansible/host_vars'
    state: 'directory'
    mode: 0755
  become: true
- name: Check if host variables file exists
  ansible.builtin.stat:
    path: '/etc/ansible/host_vars/localhost'
  register: res_host_vars_stat
  become: true
- name: Create host variables file
  ansible.builtin.file:
    path: '/etc/ansible/host_vars/localhost'
    state: 'touch'
    mode: 0644
  become: true
  when: not res_host_vars_stat.stat.exists
- name: Create satnogs-setup share directory
  ansible.builtin.file:
    path: '/usr/local/share/satnogs-setup'
    state: 'directory'
    mode: 0755
  become: true
- name: Install satnogs-setup scripts
  ansible.builtin.copy:
    src: '{{ item.0 }}'
    dest: '{{ item.1 }}'
    mode: 0755
  become: true
  with_together:
    -
      - 'usr/local/bin/satnogs-setup'
      - 'usr/local/bin/satnogs-upgrade'
      - 'usr/local/share/satnogs-setup/bootstrap.sh'
      - 'usr/local/share/satnogs-setup/config.sh'
    -
      - '/usr/local/bin/satnogs-setup'
      - '/usr/local/bin/satnogs-upgrade'
      - '/usr/local/share/satnogs-setup/bootstrap.sh'
      - '/usr/local/share/satnogs-setup/config.sh'
- import_tasks: buster_workarounds.yml
