---
- name: Add satnogs-client service user
  ansible.builtin.user:
    name: '{{ satnogs_client_user }}'
    home: '/var/lib/{{ satnogs_client_user }}'
    shell: '/bin/false'
    groups:
      - 'plugdev'
      - 'dialout'
    system: true
    state: 'present'
  become: true
  notify:
    - Restart satnogs-client service
- name: Remove deprecated satnogs-client system dependencies
  ansible.builtin.package:
    name: >-
      {{
      satnogs_client_packages |
      rejectattr("state", "undefined") |
      selectattr("state", "eq", "absent") |
      map(attribute="name") |
      list
      }}
    state: 'absent'
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  notify:
    - Restart satnogs-client service
- name: Install satnogs-client system dependencies
  ansible.builtin.package:
    name: >-
      {{
      (
      satnogs_client_packages |
      selectattr("state", "undefined") |
      map(attribute="name") |
      list
      ) +
      satnogs_client_packages |
      rejectattr("state", "undefined") |
      selectattr("state", "eq", "present") |
      map(attribute="name") |
      list
      }}
  become: true
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  notify:
    - Restart satnogs-client service
- name: Get system Python 3 version  # noqa 306
  ansible.builtin.shell: '/usr/bin/python3 --version 2>&1 | awk ''/^Python/ { print $2 }'''
  become: true
  register: res_system_python_version
  changed_when: false
- name: Get virtualenv Python version  # noqa 306
  ansible.builtin.shell: '/var/lib/{{ satnogs_client_user }}/bin/python --version 2>&1 | awk ''/^Python/ { print $2 }'''
  become: true
  register: res_virtualenv_python_version
  changed_when: false
- name: Remove virtualenv of old Python version
  ansible.builtin.file:
    path: '/var/lib/{{ satnogs_client_user }}/{{ item }}'
    state: 'absent'
  when: res_system_python_version.stdout != res_virtualenv_python_version.stdout
  become: true
  with_items:
    - 'bin'
    - 'include'
    - 'lib'
    - 'local'
    - 'share'
- name: Install PyPI dependencies
  ansible.builtin.pip:
    name: '{{ satnogs_client_pypi }}'
    virtualenv: '/var/lib/{{ satnogs_client_user }}'
    virtualenv_site_packages: true
    virtualenv_python: 'python3'
    editable: false
    extra_args: '--no-deps'
  become: true
  become_user: '{{ satnogs_client_user }}'
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  notify:
    - Restart satnogs-client service
- name: Install satnogs-client
  ansible.builtin.pip:
    name: >-
      {{
      satnogs_client_url_unstable
      if experimental and satnogs_client_url is undefined
      else satnogs_client_url | default(satnogs_client_name)
      }}
    version: '{{ satnogs_client_version if satnogs_client_url is undefined and not experimental else omit }}'
    virtualenv: '/var/lib/{{ satnogs_client_user }}'
    virtualenv_site_packages: true
    virtualenv_python: 'python3'
    editable: false
    extra_args: '--no-deps'
    state: '{{ "present" if satnogs_client_url is undefined and not experimental else "forcereinstall" }}'
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  register: res
  until: res is success
  become: true
  become_user: '{{ satnogs_client_user }}'
  notify:
    - Restart satnogs-client service
  tags:
    - satnogs_client_software
- name: Install satnogs-client configuration
  ansible.builtin.template:
    src: 'etc/default/satnogs-client.j2'
    dest: '/etc/default/satnogs-client'
    # TODO: Fix ownership
    mode: 0644
  become: true
  tags:
    - satnogs_client_config
  notify:
    - Restart satnogs-client service
- name: Add satnogs-client systemd service
  ansible.builtin.template:
    src: 'etc/systemd/system/satnogs-client.service.j2'
    dest: '/etc/systemd/system/satnogs-client.service'
    mode: 0644
  become: true
  notify:
    - Restart satnogs-client service
- name: Use tmpfs for state directory
  ansible.posix.mount:
    path: '{{ satnogs_client_state_dir }}'
    src: 'tmpfs'
    fstype: 'tmpfs'
    opts: 'rw,nosuid,noexec,nodev,noatime'
    state: 'mounted'
  when: ansible_virtualization_role != 'guest' or ansible_virtualization_type != 'docker'
  become: true
  notify:
    - Restart satnogs-client service
- name: Start satnogs-client service
  ansible.builtin.systemd:
    daemon_reload: true
    name: 'satnogs-client'
    enabled: true
    state: 'started'
  become: true
